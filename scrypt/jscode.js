function ucFirst(str) {
  if (!str) return str;
  return str[0].toUpperCase() + str.slice(1);
};
function everegeMark() {
  return +(markSumm / numberOfMarks).toFixed(2);
};
function markResult() {
  if (everegeMark() > 7) {
    console.log('%cСтуденту назначена стипендия.', "color: yellow; font-style: italic; background: linear-gradient(to right, blue, green, brown); padding: 8px; border: red 2px solid; color: #ff0; font-size: 30px; border-radius: 17px")
  }
};
function nextCource() {
  if (badMark < 1) {
    console.log('%cСтудент переведен на следующий курс.', "color: yellow; font-style: italic; background: linear-gradient(45deg, black, green, red, red, green, black); padding: 8px; border: red 2px solid; color: #ff0; font-size: 30px; border-radius: 17px")
  };
};
function createStudent() {
  student.name = ucFirst(prompt('Write your name pls', 'Ynguye'));
  student.lastName = ucFirst(prompt('Write you last name pls', 'Malmsteen'));
  student.tabel = {};
};

let student = {
  name: '',
  lastName: '',
};
createStudent();

let studentSubj;
let studentMark;
let markSumm;
let numberOfMarks;
let badMark;
markSumm = 0;
numberOfMarks = 0;
badMark = 0;

do {
  studentSubj = prompt('Enter your subject pls', 'Math');
  if (studentSubj == null) {
    break
  }
  studentMark = prompt('Enter your mark pls', '8');
  student.tabel[ucFirst(studentSubj)] = studentMark;
  markSumm += Number(studentMark);
  numberOfMarks++;
  if (studentMark < 4) {
    badMark++
  }
}
while (studentSubj != null);
nextCource();
markResult();
console.log('student', student);
console.log('badMark', badMark);
// console.log('markSumm', markSumm);
console.log('everegeMark()', everegeMark());